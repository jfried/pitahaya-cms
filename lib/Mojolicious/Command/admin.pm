package Mojolicious::Command::admin;

use Mojo::Base 'Mojolicious::Command';
use Getopt::Long qw(GetOptionsFromArray);

has description => 'Pitahaya Commands';
has usage => sub { shift->extract_usage };

sub run {
  my ( $self, $command, @args ) = @_;

  if ( $command eq "page_type" ) {
    my ( $name, $site, $desc, $create );
    GetOptionsFromArray \@args,
      's|site=s'        => sub { $site   = $_[1] },
      't|type=s'        => sub { $name   = $_[1] },
      'd|description=s' => sub { $desc   = $_[1] },
      'c|create'        => sub { $create = $_[1] };

    if ($create) {
      if ( !$name ) {
        $self->app->log->error(
          "You have to specify the name of the page type.");
        return;
      }

      if ( !$site ) {
        $self->app->log->error("You have to specify the name of the site.");
        return;
      }

      my $site_o =
        $self->app->db->resultset("Site")->search_rs( { name => $site } )->next;

      if ( !$site_o ) {
        $self->app->log->error("No site $site found.");
        return;
      }

      my $ref = {
        site_id     => $site_o->id,
        name        => $name,
        description => ( $desc || '' ),
      };

      my $new_type = $self->app->db->resultset("PageType")->create($ref);

      $self->app->log->info(
        "New page type created with id: " . $new_type->id );
    }
  }

  if ( $command eq "site" ) {
    my ( $name, $skin, $create );
    GetOptionsFromArray \@args,
      'n|name=s' => sub { $name   = $_[1] },
      's|skin=s' => sub { $skin   = $_[1] },
      'c|create' => sub { $create = $_[1] };

    if ($create) {
      $self->app->log->error("You have to specify the name of the site")
        if ( !$name );
      $self->app->log->error("You have to specify the skin of the site")
        if ( !$skin );

      $self->app->log->info("Creating new site: $name with skin: $skin");

      my $site_o = $self->app->db->resultset("Site")
        ->create( { name => $name, skin => $skin } );

      if ($site_o) {
        $self->app->log->info( "Created new site with id: " . $site_o->id );

        my $user_rs = $self->app->db->resultset("User")->search()->next;
        my $user_o;
        if ( !$user_rs ) {
          $self->app->log->info("No users found. Creating new one.");
          $user_o = $self->app->db->resultset("User")->create(
            {
              username => "admin",
              password => "admin",
            }
          );

          $self->app->log->info("Created new user admin with password admin.");
        }
        else {
          $user_o = $user_rs;
        }

        my ( @page_types, @media_types );

        for my $page_type (qw/index page/) {
          $self->app->log->info("Creating page type: $page_type");

          push @page_types,
            $self->app->db->resultset("PageType")->create(
            {
              site_id => $site_o->id,
              name    => $page_type,
            }
            );
        }

        for my $media_type (qw/index folder image object/) {
          $self->app->log->info("Creating media type: $media_type");

          push @media_types,
            $self->app->db->resultset("MediaType")->create(
            {
              site_id => $site_o->id,
              name    => $media_type,
            }
            );
        }

        $self->app->log->info("Creating root page");
        my $root_page = $self->app->db->resultset("Page")->create(
          {
            site_id    => $site_o->id,
            lft        => 1,
            rgt        => 2,
            level      => 0,
            hidden     => 0,
            navigation => 1,
            active     => 1,
            name       => 'Home',
            url        => 'Home',
            type_id    => $page_types[0]->id,
            creator_id => $user_o->id,
          }
        );

        $self->app->log->info("Creating root media folder");
        my $root_media = $self->app->db->resultset("Media")->create(
          {
            site_id    => $site_o->id,
            lft        => 1,
            rgt        => 2,
            level      => 0,
            hidden     => 0,
            active     => 1,
            name       => 'Root',
            url        => 'Root.html',
            type_id    => $media_types[0]->id,
            creator_id => $user_o->id,
          }
        );

        $self->app->log->info( "Page " . $site_o->name . " created." );
      }
    }
  }
}

1;
